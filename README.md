# Quaternion Institute mdBook Theme
## Usage
1. Use an existing book or create a new one: `mdbook init my_book`
2. Go into the new directory: `cd my_book`
3. Create a new Git repository: `git init`
4. Clone this repo as a submodule: `git submodule add https://gitlab.com/quaternion-inst/courses/theme`
5. Add the following to `book.toml`:
```Toml
[output.html]
additional-css = ["theme/quaternion.css"]
default-theme = "Quaternion"
curly-quotes = true
```
6. If the course needs math rendering, install [mdbook-katex](https://github.com/lzanini/mdbook-katex) and add `[preprocessor.katex]` to `book.toml`.
**Note**: mdbook-katex's dependencies can have some issues, for testing you can use [MathJax](https://rust-lang.github.io/mdBook/format/mathjax.html).

### Cloning a Book Repo
If you are cloning an existing book using this theme, be sure to use `--recursive`: `git clone git@gitlab.com:quaternion-inst/courses/$BOOK.git --recursive`.

## Updating
This theme will be updated occasionally, to fetch the latest version use `git submodule update --remote` in your book directory.
